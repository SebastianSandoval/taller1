﻿**Request**: Es un mensaje de solicitud generado por el cliente para el servidor. [Fuente](https://tools.ietf.org/html/rfc2616#page-35)
**Response**: Es la respuesta que se realizar, luego de recibir e interpretar el request enviado. [Fuente](https://tools.ietf.org/html/rfc2616#page-39)
**Status Codes**: Es el resultado generado en un valor entero, luego de comprender y realizar la request. [Fuente](https://tools.ietf.org/html/rfc2616#page-39)
**Get**: Este metodo se ocupa para recuperar datos, donde solicita una representacion de un recurso. [Fuente]https://developer.mozilla.org/es/docs/Web/HTTP/Methods)
**Post**: Este metodo se utiliza para enviar una entidad a un recurso especifico. [Fuente](https://developer.mozilla.org/es/docs/Web/HTTP/Methods)
**Head**: Este metodo solicita una respuesta como el metodo get, solamente que el resultado no contiene el cuerpo de la respuesta. [Fuente](https://developer.mozilla.org/es/docs/Web/HTTP/Methods)
**Options**: Este metodo describe las opciones de comunicacion con el recurso de destino. [Fuente](https://developer.mozilla.org/es/docs/Web/HTTP/Methods)
**Put**: Este metodo crea un nuevo recurso o remplaza una representacion de un recurso de destino con la request necesitada. [Fuente](https://developer.mozilla.org/es/docs/Web/HTTP/Methods)
**Delete**: Este metodo elimina un recurso especifico. [Fuente](https://developer.mozilla.org/es/docs/Web/HTTP/Methods)
**Header**: Permite al cliente y el servidor, pasar informacion adicional con la request o response. [Fuente](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers)
**Accept**: Este campo se utiliza para indicar que la request está limitada a un pequeño conjunto de tipos requeridos. [Fuente](https://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html)
**Accept-Charset**: Este campo se usa para indicar los conjuntos de caracteres que son aceptables para el response. [Fuente](https://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html)
**Accept-Encoding**: Este campo es similar al campo Accept, solamente que este restringe las codificaciones de contenido que son aceptables para el response. [Fuente](https://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html)        
**Cache-Control** : Este campo se utiliza para especificar directivas que "DEBEN" ser obedecidas por todos los mecanismos de almacenamiento en caché a lo largo de la cadena de solicitud / respuesta. [Fuente](https://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html)   
**Connection** : Este campo permite al usuario definir las opciones para la conexión. [Fuente](https://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html)     
**Cookie** : Esto es una pequeña cantidad de información, la cual es enviada por un sitio web y almacenada por el navegador del usuario, para que asi el sitio web puede conocer la actividad previa del usuario. [Fuente](https://developer.mozilla.org/es/docs/Web/HTTP/Headers/Cookie)
**Set-Cookie** : Mediante este set se pueden enviar cookies desde el servidor al usuario. [Fuente](https://developer.mozilla.org/es/docs/Web/HTTP/Headers/Set-Cookie) 
**Host** : Especifica el nombre deL dominio del servidor y ademas el número del puerto tcp del servidor que esta escuchando. [Fuente](https://developer.mozilla.org/es/docs/Web/HTTP/Headers/Host) 
**Origin** : Indica el origen de la recolección e inicia una request para servidores con respuesta al access-control-allow-origin. [Fuente](https://es.wikipedia.org/wiki/Anexo:Lista_de_cabeceras_HTTP) 
**Referer** : Indica la url del cual proviene. [Fuente](https://es.wikipedia.org/wiki/Anexo:Lista_de_cabeceras_HTTP)
**Content-Encoding** : Se utiliza para comprimir el media-type el cual es un identificador de formatos de archivo y contenidos que son transmitidos en Internet.[Fuente](https://developer.mozilla.org/es/docs/Web/HTTP/Headers/Content-Encoding) 
**Content-Length** : Indica el tamaño del cuerpo de la entidad, que fue enviado al receptor. [Fuente](https://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html) 
**Content-Type** : Indica el medio del cuerpo de la entidad enviada al receptor. [Fuente](https://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html) 
**Location** : Puede redirigir al receptor a una ubicación distinta de la request-uri. [Fuente](https://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html)  
**Upgrade** : Permite al cliente definir los protocolos de comunicación adicionales. [Fuente](https://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html) 

| REQ/RES  | Método HTTP   | URL               |    HEADERS      |         STATUS     |   Descripción  |
| ---------|:-------------:| -----------------:|:--------------: |:-----------------: |:-------------: |
|REQ|GET|http://www.inf.ucv.cl/~ifigueroa|Upgrade-Insecure-Requests, User-Agent, Accept, Accept-Encoding, Accept-Language, Cookie|N/A|La request fue enviada correctamente y se devuelve al sitio|
|RES|N/A|http://zeus.inf.ucv.cl/~ifigueroa|Date,Server,Location,Content-Length,Connection,Content-Type|301|La request fue enviada correctamente y se devuelve al sitio|
|REQ|GET|http://zeus.inf.ucv.cl/~ifigueroa|Upgrade-Insecure-Requests,User-Agent,Accept,Accept-Encoding,Accept-Language,Cookie|N/A|La request fue enviada correctamente y se devuelve al sitio|
|RES|N/A|http://zeus.inf.ucv.cl/~ifigueroa/|Date,Server,Accept-Encoding,Content-Encoding,Content-Length,Keep-Alive,Connection,Content-Type|301|La request fue enviada correctamente y se devuelve al sitio|
|REQ|GET|http://zeus.inf.ucv.cl/~ifigueroa/|Upgrade-Insecure-Requests,User-Agent,Accept,Accept-Encoding,Accept-Language,Cookie|N/A|La request fue enviada correctamente y se devuelve al sitio|
|RES|N/A|http://zeus.inf.ucv.cl/~ifigueroa/doku.php|Date,Server,Accept-Encoding,Content-Encoding,Content-Length,Keep-Alive,Connection,Content-Type|302|Comienza la petición del sitio. Verifica las cookies previamente|
|REQ|GET|http://zeus.inf.ucv.cl/~ifigueroa/doku.php|User-Agent,Upgrade-Insecure-Requests,Accept,Accept-Encoding,Accept-Language,Cookie|N/A|Comienza la petición del sitio. Verifica las cookies previamente|
|RES|N/A|http://zeus.inf.ucv.cl/~ifigueroa/lib/tpl/bootstrap3/assets/bootstrap/default/bootstrap.min.css|Date,Server,Cache-Control,Pragma,X-UA-Compatible,Set-Cookie,Accept-Encoding,Content-Encoding,Content-Length,Keep-Alive,Connection,Content-Type|200|Se inicia la petición de los estilos css|
|REQ|GET|http://zeus.inf.ucv.cl/~ifigueroa/lib/tpl/bootstrap3/assets/bootstrap/default/bootstrap.min.css|User-Agent,Accept,Referer,Accept-Encoding,Accept-Language,Cookie|N/A|Se inicia la petición de los estilos css|
|RES|N/A|http://zeus.inf.ucv.cl/~ifigueroa/lib/exe/css.php?t=bootstrap3&tseed=98b7e28310a0033fb1fdcb6e46b08ea5|Date,Server,Last-Modified,ETag,Accept-Ranges,Vary,Accept-Encoding,Content-Encoding,Content-Length,Keep-Alive,Connection,Content-Type,|200|Se inicia la petición de los estilos css|
|REQ|GET|http://zeus.inf.ucv.cl/~ifigueroa/lib/exe/css.php?t=bootstrap3&tseed=98b7e28310a0033fb1fdcb6e46b08ea5|User-Agent,,Accept,Referer,Accept-Encoding,Accept-Language,Cookie|N/A|Se inicia la petición de los estilos css|
|RES|N/A|http://zeus.inf.ucv.cl/~ifigueroa/lib/tpl/bootstrap3/assets/font-awesome/css/font-awesome.min.css|Date,Server,X-Powered-By,Cache-Control,Pragma,ETag,Last-Modified,Vary,Accept-Encoding,Content-Encoding,Content-Length,Keep-Alive,Connection,Content-Type|200|Se inicia la petición de los estilos css que contienen las fuentes y tipos de letras|
|REQ|GET|http://zeus.inf.ucv.cl/~ifigueroa/lib/tpl/bootstrap3/assets/font-awesome/css/font-awesome.min.css|User-Agent,Accept,Referer,Accept-Encoding,Accept-Language,Cookie|N/A|Se inicia la petición de los estilos css que contienen las fuentes y tipos de letras|
|RES|N/A|http://zeus.inf.ucv.cl/~ifigueroa/lib/exe/jquery.php?tseed=23f888679b4f1dc26eef34902aca964f|Date,Server,Last-Modified,ETag,Accept-Ranges,Vary,Accept-EncodingContent-Encoding,Content-Length,Keep-Alive,Connection,Content-Type|200|Se inicia la solicitu del archivo jquery|
|REQ|GET|http://zeus.inf.ucv.cl/~ifigueroa/lib/exe/jquery.php?tseed=23f888679b4f1dc26eef34902aca964f|User-Agent:,Accept,Referer,Accept-Encoding,Accept-Language,Cookie|N/A|Se inicia la solicitu del archivo jquery|
|RES|N/A|http://zeus.inf.ucv.cl/~ifigueroa/lib/exe/js.php?t=bootstrap3&tseed=98b7e28310a0033fb1fdcb6e46b08ea5||200|Se inicia la solicitu del archivo jquery|
|REQ|GET|http://zeus.inf.ucv.cl/~ifigueroa/lib/exe/js.php?t=bootstrap3&tseed=98b7e28310a0033fb1fdcb6e46b08ea5|User-Agent,Accept,Referer,Accept-Encoding,Accept-Language,Cookie|N/A|Se inicia la petición de la imagen con extension .js para ser cargarda en el sitio|
|RES|N/A|http://zeus.inf.ucv.cl/~ifigueroa/lib/tpl/bootstrap3/assets/bootstrap/js/bootstrap.min.js|Date,Server,X-Powered-By,Cache-Control,Pragma,ETag,Last-Modified,Vary,Accept-Encoding,Content-Encoding,Content-Length,Keep-Alive,Connection,Content-Type|200|Se inicia la solicitu del archivo js|
|REQ|GET|http://zeus.inf.ucv.cl/~ifigueroa/lib/tpl/bootstrap3/assets/bootstrap/js/bootstrap.min.js|User-Agent,Accept,Referer,Accept-Encoding,Accept-Language,Cookie|N/A|Se inicia la petición de la imagen con extension .js para ser cargarda en el sitio|
|RES|N/A|http://zeus.inf.ucv.cl/~ifigueroa/lib/tpl/bootstrap3/assets/anchorjs/anchor.min.js|Date,Server,Last-Modified,ETag,Accept-Ranges,Vary,Accept-Encoding,Content-Encoding,Content-Length,Keep-Alive,Connection,Content-Type|200|Se inicia la petición de la imagen con extension .js para ser cargarda en el sitio|
|REQ|GET|http://zeus.inf.ucv.cl/~ifigueroa/lib/tpl/bootstrap3/assets/anchorjs/anchor.min.js|User-Agent,Accept,Referer,Accept-Encoding,Accept-Language,Cookie|N/A|Se inicia la petición de la imagen con extension .js para ser cargarda en el sitio|
|RES|N/A|http://zeus.inf.ucv.cl/~ifigueroa/lib/tpl/bootstrap3/images/logo.png|Date,Server,Last-Modified,ETag,Accept-Ranges,Vary,Accept-Encoding,Content-Encoding,Content-Length,Keep-Alive,Connection,Content-Type|200|Se inicia la petición de la imagen con extension .png para ser cargarda en el sitio|
|REQ|GET|http://zeus.inf.ucv.cl/~ifigueroa/lib/tpl/bootstrap3/images/logo.png|User-Agent,Accept,Referer,Accept-Encoding,Cookie|N/A|Se inicia la petición de la imagen con extension .png para ser cargarda en el sitio|
|RES|N/A|http://zeus.inf.ucv.cl/~ifigueroa/lib/tpl/bootstrap3/images/button-bootstrap3.png|Date,Server,Last-Modified,ETag,Accept-Ranges,Content-Length,Keep-Alive,Connection,Content-Type|200|Se inicia la petición de la imagen con extension .png para ser cargarda en el sitio|
|REQ|GET|http://zeus.inf.ucv.cl/~ifigueroa/lib/tpl/bootstrap3/images/button-bootstrap3.png|User-Agent,Accept,Referer,Accept-Encoding,Accept-Language,Cookie|N/A|Se inicia la petición de la imagen con extension .png para ser cargarda en el sitio|
|RES|N/A|http://zeus.inf.ucv.cl/~ifigueroa/lib/tpl/dokuwiki/images/button-php.gif|Date,Server,Last-Modified,ETag,Accept-Ranges,Content-Length,Keep-Alive,Connection,Content-Type|200|Se inicia la petición de la imagen con extension .gif para ser cargarda en el sitio|
|REQ|GET| http://zeus.inf.ucv.cl/~ifigueroa/lib/tpl/dokuwiki/images/button-php.gif|User-Agent,Accept,Referer,Accept-Encoding,Accept-Language,Cookie|N/A|Se inicia la petición de la imagen con extension .gif para ser cargarda en el sitio|
|RES|N/A|http://zeus.inf.ucv.cl/~ifigueroa/lib/tpl/dokuwiki/images/button-html5.png|Date,Server,Last-Modified,ETag,Accept-Ranges,Content-Length,Keep-Alive,Connection,Content-Type|200|Se inicia la petición de la imagen con extension .png para ser cargarda en el sitio|
|REQ|GET|http://zeus.inf.ucv.cl/~ifigueroa/lib/tpl/dokuwiki/images/button-html5.png|User-Agent,Accept,Referer,Accept-Encoding,Accept-Language,Cookie|N/A|Se inicia la petición de la imagen con extension .png para ser cargarda en el sitio|
|RES|N/A|http://zeus.inf.ucv.cl/~ifigueroa/lib/tpl/dokuwiki/images/button-css.png|Date,Server,Last-Modified,ETag,Accept-Ranges,Content-Length,Keep-Alive,Connection,Content-Type|200|Se inicia la petición de los estilos css|
|REQ|GET|http://zeus.inf.ucv.cl/~ifigueroa/lib/tpl/dokuwiki/images/button-css.png|User-Agent,Accept,Referer,Accept-Encoding,Accept-Language,Cookie|N/A|Se inicia la petición de los estilos css y ademas se hace uso de las cookies|
|RES|N/A|http://zeus.inf.ucv.cl/~ifigueroa/lib/tpl/dokuwiki/images/button-dw.png|Date,Server,Last-Modified,ETag,Accept-Ranges,Content-Length,Keep-Alive,Connection,Content-Type|200|Se inicia la petición de la imagen con extension .png para ser cargarda en el sitio|
|REQ|GET|http://zeus.inf.ucv.cl/~ifigueroa/lib/tpl/dokuwiki/images/button-dw.png|User-Agent,Accept,Referer,Accept-Encoding,Accept-Language,Cookie|N/A|Se inicia la petición de la imagen con extension .png para ser cargarda en el sitio|
|RES|N/A|http://zeus.inf.ucv.cl/~ifigueroa/lib/tpl/bootstrap3/assets/font-awesome/fonts/fontawesome-webfont.woff2?v=4.7.0|Date,Server,Last-Modified,ETag,Accept-Ranges,Content-Length,Keep-Alive,Connection,Content-Type|200|Se envia la petición de la fuente .woff2 para ser utilizada en el sitio web|
|REQ|GET|http://zeus.inf.ucv.cl/~ifigueroa/lib/tpl/bootstrap3/assets/font-awesome/fonts/fontawesome-webfont.woff2?v=4.7.0|User-Agent,Origin,Accept,Referer,Accept-Encoding,Accept-Language,Cookie|N/A|Se envia la petición de la fuente .woff2 para ser utilizada en el sitio web|
|RES|N/A|http://zeus.inf.ucv.cl/~ifigueroa/lib/tpl/bootstrap3/assets/bootstrap/fonts/glyphicons-halflings-regular.woff2|Date,Server,Last-Modified,ETag,Accept-Ranges,Content-Length,Keep-Alive,Connection,Content-Type|200|Se envia la petición de la fuente .woff2 para ser utilizada en el sitio web|
|REQ|GET|http://zeus.inf.ucv.cl/~ifigueroa/lib/tpl/bootstrap3/assets/bootstrap/fonts/glyphicons-halflings-regular.woff2|User-Agent,Origin,Accept,Referer,Accept-Encoding,Accept-Language,Cookie|N/A|Se envia la petición de la fuente .woff2 para ser utilizada en el sitio web|
|RES|N/A|http://zeus.inf.ucv.cl/~ifigueroa/lib/plugins/note/images/important.png|Date,Server,Last-Modified,ETag,Accept-Ranges,Content-Length,Keep-Alive,Connection,Content-Type|200|Se inicia la petición de la imagen con extension .png para ser cargarda en el sitio|
|REQ|GET|http://zeus.inf.ucv.cl/~ifigueroa/lib/plugins/note/images/important.png|User-Agent,Accept,Referer,Accept-Encoding,Accept-Language,Cookie|N/A|Se inicia la petición de la imagen con extension .png para ser cargarda en el sitio|
|RES|N/A|http://zeus.inf.ucv.cl/~ifigueroa/lib/exe/indexer.php?id=start&1502889044|Date,Server,Last-Modified,ETag,Accept-Ranges,Content-Length,Keep-Alive,Connection,Content-Type|200|La request fue enviada correctamente y se devuelve al sitio|
|REQ|GET|http://zeus.inf.ucv.cl/~ifigueroa/lib/exe/indexer.php?id=start&1502889044|User-Agent,Accept,Referer,Accept-Encoding,Accept-Language,Cookie|N/A|La request fue enviada correctamente y se devuelve al sitio|
|RES|N/A|http://zeus.inf.ucv.cl/~ifigueroa/lib/tpl/bootstrap3/images/favicon.ico|Date,Server,X-Powered-By,Expires,Cache-Control,Pragma,Connection,Set-Cookie,Content-Length,Content-Type|200|Se realiza una petición por la imagen con extension .ico dada por bootstrap para cargarla al sitio.|
|REQ|GET|http://zeus.inf.ucv.cl/~ifigueroa/lib/tpl/bootstrap3/images/favicon.ico|User-Agent,Accept,Referer,Accept-Encoding,Accept-Language,Cookie|N/A|Se realiza una petición por la imagen con extension .ico dada por bootstrap para cargarla al sitio.|
|RES|N/A|N/A|Date,Server,Last-Modified,ETag,Accept-Ranges,Content-Length,Keep-Alive,Connection,Content-Type|200|La request fue enviada correctamente y se devuelve al sitio|

>Al ingresar a la direccion url (http://www.inf.ucv.cl/~ifigueroa) en el navegador chrome, verificando previamente las cookies almacenadas en el equipo, el servidor web respondio de forma correcta.

| Req/Res | Metodo HTTP | URL | Headers | Status | Descripción |
|:---------:|:-------------:|:-------------------------------------------------------------------------------------------------------------:|:---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------:|:--------:|:-------------:|
| Req | Get | http://www.jaidefinichon.com/ | Upgrade-Insecure-Request, User-Agent, Accept, Accept-Encoding, Accept-Language | N/A |  |
| Res | N/A | N/A | Location, Date, Content-Type, Server, Content-Lenght, X-XSS-Protection, X-Frame-Options | 301 |  |
| Req | Get | http://www.jaidefinichon.com/ | Upgrade-Insecure-Request, User-Agent, Accept, Accept-Encoding, Accept-Language | N/A |  |
| Res | N/A | N/A | Expires, Date, Content-Type, Cache-Control, Last-Modified, ETag, Content-Encoding, Server, Content-Lenght, X-Content-Type-Options, X-XSS-Protection, Content-Lenght, Server | 200 |  |
| Req | Get | https://www.blogger.com/static/v1/widgets/763856997-widget_css_bundle.css | User-Agent, Accept, Referer, Accept-Encoding, Accept-Language | N/A |  |
| Res | N/A | N/A | Status, Accept-Ranges, Vary, Content-Encoding, Content-Type, Content-Lenght, Date, Expires, Last-Modified, X-Content-Type-Options, server, x-xss-protection, cache-control, age, alt-svc | 200 |  |
| Req | Get | http://jaidefinichon.org/ | Upgrade, User-Agent, Accept, Referer, Accept-Encoding, Accept-Language | N/A |  |
| Res | N/A | N/A | Server, Date, Content-Type, Content-Lenght, Connection, Vary, P3P, X-XSS-Protection, X-Content-Type, X-Tumblr-User, X-Tumblr-Content, Rating, X-UA, Vary, Accept | 200 |  |
| Req | Get | http://jaidefinichon.org/ | Upgrade-Insecure-Request, User-Agent, Accept, Referer, Accept-Encoding, Accept-Lanuage | N/a |  |
| Res | N/A | N/A | Server, Date, Content-Type, Content-Lenght, Connection, Vary, P3P, X-XSS-Protection, X-Content-Options, X-Tumblr-User, X-Tumblr-Content-Rating, Rating, X-Tumblr-Pixel, Link, X-UA-Compatible, Content-Encoding, X-UA-Device, Vary, Accept-Ranges | 200 |  |
| Req | Get | http://assets.tumblr.com/assets/scripts/pre_tumblelog.js?_v=7e0654d636b56bfe6a0970b99e23e0f7 | User-Agent, Accept, Referer, Accept-Encoding, Accept-Language | N/A |  |
| Res | N/A | N/A | Server, Date, Content-Type, Vary, Last-Modified, ETag, Content-Encoding, Expires, Cache-Control, Strict-Transport-Security, Timing-Allow-Origin, Access-Control-Allow-Origin, Age, Content-Lenght, Via, Connection | 200 |  |
| Req | Get | http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,700italic,800italic,400,700,600,800 | User-Agent, Accept, Referer, Accept-Encoding, Accept-Language | N/A |  |
| Res | N/A | N/A | Content-Type, Access-Control-Allow-Origin, Timing-Allow-Origin, Link, Expires, Date, Cache-Control, Last-Modified, Content-Encoding, Server, X-XSS-Protection, X-Frame-Options | 200 |  |
| Req | Get | https://www.googletagservices.com/tag/js/gpt.js | User-agent, Accept, Referer, Accept-Encoding, Accept-Language | N/A |  |
| Res | N/A | N/A | Status, Accept-Ranges, Vary, Content-Encoding, Content-Tyep | 200 |  |
| Req | Get | http://assets.tumblr.com/assets/scripts/tumblelog_post_message_queue.js?_v=a8938c0e77cf8b1347c2e8acd1ee607c | User-agent, Accept, Referer, Accept-Encoding, Accept-Language | N/A |  |
| Res | N/A | N/A | Server, Date, Content-Type, Vary, Last-Modified, ETag, Content-Encoding, Expires, Cache-Control, Strict-Transport-Security, Timing-Allow-Origin, Access-Control-Allow-Origin, Age, Content-Lenght, Via, Connection | 200 |  |
| Req | Get | http://assets.tumblr.com/fonts/gibson/stylesheet.css?v=3 | User-agent, Accept, Referer, Accept-Encoding, Accept-Language | N/A |  |
| Res | N/A | N/A | Server, Date, Content-Type, Last-Modified, Vary, ETag, Content-Encoding, Expires, Cache-Control, Strict-Transport-Security, Timing-Allow-Origin, Access-Control-Allow-Origin, Age, Content-Lenght, Via, Connection | 200 |  |

 Al ingresar a la direccion url (http://www.jaidefinichon.com) en el navegador chrome, verificando previamente las cookies almacenadas en el equipo, el servidor web respondio de forma correcta 




